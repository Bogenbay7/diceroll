//
//  ViewController.swift
//  DiceRoll
//
//  Created by Nurba on 14.04.2021.
//

import UIKit

protocol SecondDelegate {
    func pass()
}

class ViewController: UIViewController {
    let diceArray = ["dice1", "dice2", "dice3", "dice4", "dice5", "dice6"];
    public var rolls: [(first: Int , second: Int)] = [(Int, Int)]()


    var delegate: SecondViewController!
    
    @IBOutlet weak var rollTapped: UIButton!
    @IBOutlet weak var diceImage1: UIImageView!
    @IBOutlet weak var diceImage2: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rollTapped.layer.masksToBounds = true
     
    }
    override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        let numberOne = Int(arc4random()) % 6

        let numberTwo = Int(arc4random()) % 6
             
  
             
        diceImage1.image = UIImage(named: diceArray[numberOne])
             
        diceImage2.image = UIImage(named: diceArray[numberTwo])
        
        rolls.append((first: numberOne, second: numberTwo))
    }

    @IBAction func openSV(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(identifier: "rollsNum") as! SecondViewController
        vc.modalPresentationStyle = .fullScreen
       
        let numberOne = Int(arc4random()) % 6

        let numberTwo = Int(arc4random()) % 6
    
        vc.firstLab = numberOne
        vc.secondLab = numberTwo
        vc.rolls = rolls
        
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func rollAction(_ sender: Any) {
        
  
        
        let numberOne = Int(arc4random()) % 6

        let numberTwo = Int(arc4random()) % 6

             
        diceImage1.image = UIImage(named: diceArray[numberOne])
             
        diceImage2.image = UIImage(named: diceArray[numberTwo])
        
        rolls.append((first: numberOne, second: numberTwo))
        
       
    }
}

